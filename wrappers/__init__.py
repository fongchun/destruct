import delly
import destruct
import lumpysv

catalog = dict()
catalog['delly'] = delly.DellyWrapper
catalog['destruct'] = destruct.DestructWrapper
catalog['lumpysv'] = lumpysv.LumpySVWrapper

